#include <stdio.h>
#include <iostream>
#include <cstring>
#include <chrono>
#include <thread>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <mutex>
#include <atomic>
#include <future>
#include "../include/HCNetSDK.h"

using namespace std;

//时间解析宏定义
#define GET_YEAR(_time_)    (((_time_)>>26) + 2000)
#define GET_MONTH(_time_) (((_time_)>>22) & 15)
#define GET_DAY(_time_) (((_time_)>>17) & 31)
#define GET_HOUR(_time_) (((_time_)>>12) & 31)
#define GET_MINUTE(_time_)  (((_time_)>>6) & 63)
#define GET_SECOND(_time_)  (((_time_)>>0) & 63)

const char* fifo = "/tmp/camera";
int fd;
std::mutex mu;

BOOL CALLBACK MessageCallback(LONG lCommand, NET_DVR_ALARMER *pAlarmer, char *pAlarmInfo, DWORD dwBufLen, void* pUser) {

    switch(lCommand) {
        case COMM_ALARM_FACE_DETECTION: //人脸侦测报警信息
            {
                NET_DVR_FACE_DETECTION struFaceDetectionAlarm = {0};
                memcpy(&struFaceDetectionAlarm, pAlarmInfo, sizeof(NET_DVR_FACE_DETECTION));
                NET_DVR_TIME struAbsTime = {0};
                struAbsTime.dwYear = GET_YEAR(struFaceDetectionAlarm.dwAbsTime);
                struAbsTime.dwMonth = GET_MONTH(struFaceDetectionAlarm.dwAbsTime);
                struAbsTime.dwDay = GET_DAY(struFaceDetectionAlarm.dwAbsTime);
                struAbsTime.dwHour = GET_HOUR(struFaceDetectionAlarm.dwAbsTime);
                struAbsTime.dwMinute = GET_MINUTE(struFaceDetectionAlarm.dwAbsTime);
                struAbsTime.dwSecond = GET_SECOND(struFaceDetectionAlarm.dwAbsTime);
                //保存抓拍场景图片
                if (struFaceDetectionAlarm.dwBackgroundPicLen > 0 && struFaceDetectionAlarm.pBackgroundPicpBuffer != NULL) {
                    char cFilename[256] = {0};
                    char chTime[128];
                    //sprintf(chTime,"%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d",struAbsTime.dwYear,struAbsTime.dwMonth,struAbsTime.dwDay,struAbsTime.dwHour,struAbsTime.dwMinute,struAbsTime.dwSecond)
                    sprintf(cFilename, "FaceDetectionBackPic[%s][%s].jpg",struFaceDetectionAlarm.struDevInfo.struDevIP.sIpV4, chTime);
                    // hFile = CreateFile(cFilename, GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
                    // if (hFile == INVALID_HANDLE_VALUE) {
                    //     break;
                    // }
                    // WriteFile(hFile, struFaceDetectionAlarm.pBackgroundPicpBuffer, struFaceDetectionAlarm.dwBackgroundPicLen, &dwReturn, NULL);
                    // CloseHandle(hFile);
                    // hFile = INVALID_HANDLE_VALUE;
                }
                printf("人脸侦测报警[0x%x]: Abs[%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d] Dev[ip:%s,port:%d,ivmsChan:%d] \n",\
                        lCommand, struAbsTime.dwYear, struAbsTime.dwMonth, struAbsTime.dwDay, struAbsTime.dwHour, \
                        struAbsTime.dwMinute, struAbsTime.dwSecond, struFaceDetectionAlarm.struDevInfo.struDevIP.sIpV4, \
                        struFaceDetectionAlarm.struDevInfo.wPort, struFaceDetectionAlarm.struDevInfo.byIvmsChannel);
            }
            case COMM_UPLOAD_FACESNAP_RESULT:
            {
                NET_VCA_FACESNAP_RESULT structFaceSnap= {0};
                memcpy(&structFaceSnap, pAlarmInfo, sizeof(NET_DVR_FACE_DETECTION));
                NET_DVR_TIME struAbsTime = {0};
                struAbsTime.dwYear = GET_YEAR(structFaceSnap.dwAbsTime);
                struAbsTime.dwMonth = GET_MONTH(structFaceSnap.dwAbsTime);
                struAbsTime.dwDay = GET_DAY(structFaceSnap.dwAbsTime);
                struAbsTime.dwHour = GET_HOUR(structFaceSnap.dwAbsTime);
                struAbsTime.dwMinute = GET_MINUTE(structFaceSnap.dwAbsTime);
                struAbsTime.dwSecond = GET_SECOND(structFaceSnap.dwAbsTime);
                memcpy(&structFaceSnap, pAlarmInfo, sizeof(NET_VCA_FACESNAP_RESULT));
                cout<<structFaceSnap.struDevInfo.struDevIP.sIpV4<<endl;
                if (structFaceSnap.dwFacePicLen > 0 && structFaceSnap.pBuffer1 != NULL){
                    cout<<structFaceSnap.dwFacePicLen<<endl;
                    cout<<"open pipe for write"<<endl;

                    // if ((fd = open(fifo, O_WRONLY)) < 1){
                    //     perror("Error open pipe");
                    //     return 1;
                    // }
                    ofstream fd;
                    // fd << std::to_string(structFaceSnap.dwFacePicLen) << endl;
                    fd.open(fifo, ios::out | ios::app | ios::binary);
                    mu.lock();
                    cout<<"writing to pipe"<<endl;
                    cout<<structFaceSnap.sStorageIP<<endl;
                    fd.write((char*)&structFaceSnap.struDevInfo.struDevIP.sIpV4[0], 16*sizeof(char));
                    fd.write((char*)&structFaceSnap.pBuffer1[0], structFaceSnap.dwFacePicLen);
                    // write(fd, structFaceSnap.pBuffer1, structFaceSnap.dwFacePicLen);
                    cout<<"data written"<<endl;
                    mu.unlock();

                    // char cFilename[256] = {0};
                    // char chTime[128];
                    // sprintf(chTime,"%4.4d%2.2d%2.2d%2.2d%2.2d%2.2d",struAbsTime.dwYear,struAbsTime.dwMonth,struAbsTime.dwDay,struAbsTime.dwHour,struAbsTime.dwMinute,struAbsTime.dwSecond);
                    // sprintf(cFilename, "FaceDetectionBackPic[%s].jpg", chTime);
                    // cout << cFilename <<endl;
                    // auto imgFile = fstream(cFilename, ios::out | ios::binary);
                    // imgFile.write((char*)&structFaceSnap.pBuffer1[0], structFaceSnap.dwFacePicLen);
                }
            }
            break;
        default:
            printf("其他报警,报警信息类型: 0x%x\n", lCommand);
            break;
    }
    return TRUE;
}

inline bool file_exists(const std::string& name) {
  struct stat buffer;   
  return (stat (name.c_str(), &buffer) == 0); 
}

class CameraListener{
    public:
        CameraListener(const char* host, int port, const char* username, const char* pass){
            NET_DVR_USER_LOGIN_INFO struLoginInfo = {0};
            struLoginInfo.bUseAsynLogin = 0; //同步登录方式
            strcpy(struLoginInfo.sDeviceAddress, host); //设备IP地址
            struLoginInfo.wPort = port; //设备服务端口
            strcpy(struLoginInfo.sUserName, username); //设备登录用户名
            strcpy(struLoginInfo.sPassword, pass); //设备登录密码
            //设备信息, 输出参数
            NET_DVR_DEVICEINFO_V40 struDeviceInfoV40 = {0};
            lUserID = NET_DVR_Login_V40(&struLoginInfo, &struDeviceInfoV40);
            if (lUserID < 0) {
                printf("Login failed, error code: %d\n", NET_DVR_GetLastError());
                // NET_DVR_Cleanup();
            }
        }
        void start(){
            this->task_ = std::thread(&CameraListener::listenFaceCapture, this);
        }

        void stop(){
            this->isRunning_ = false;
            task_.join();
        }

        ~CameraListener(){
            NET_DVR_Logout(lUserID);
            NET_DVR_Cleanup();
            this->stop();
        }

    private:
        std::atomic<bool> isRunning_;
        std::thread task_;
        LONG lUserID;
        LONG lHandle;

        void listenFaceCapture(){
            //设置报警回调函数
            NET_DVR_SetDVRMessageCallBack_V31(MessageCallback, NULL);
            //启用布防
            LONG lHandle;
            NET_DVR_SETUPALARM_PARAM struAlarmParam={0};
            struAlarmParam.dwSize=sizeof(struAlarmParam);
            struAlarmParam.byFaceAlarmDetection = 1; //人脸侦测报警,设备支持人脸侦测功能的前提下,上传COMM_ALARM_FACE_DETECTION类型报警信息
            //其他报警布防参数不需要设置,不支持
            lHandle = NET_DVR_SetupAlarmChan_V41(lUserID, & struAlarmParam);
            if (lHandle < 0) {
                printf("NET_DVR_SetupAlarmChan_V41 error, %d\n", NET_DVR_GetLastError());
                NET_DVR_Logout(lUserID);
                NET_DVR_Cleanup();
            }
        }
};

int main() {
    //---------------------------------------
    //初始化
    NET_DVR_Init();
    //设置连接时间与重连时间 
    NET_DVR_SetConnectTime(2000, 1);
    NET_DVR_SetReconnect(10000, true);
    //---------------------------------------
    //注册设备
    //
    if (file_exists(fifo)){
	if(unlink(fifo) < 0){
	    perror("Error remove existing fifo");
	    return 1;
	}
    }

    if (mkfifo(fifo, 0666) <0) { // create named file for named pipe
	perror("Error: mkfifo");
	return 1;
    }
    CameraListener worker("192.100.1.112", 8000, "admin", "1111aaaa");
    worker.start();
    // LONG lUserID;

    // //登录参数,包括设备地址、登录用户、密码等
    // NET_DVR_USER_LOGIN_INFO struLoginInfo = {0};
    // struLoginInfo.bUseAsynLogin = 0; //同步登录方式
    // strcpy(struLoginInfo.sDeviceAddress, "192.100.1.112"); //设备IP地址
    // struLoginInfo.wPort = 8000; //设备服务端口
    // strcpy(struLoginInfo.sUserName, "admin"); //设备登录用户名
    // strcpy(struLoginInfo.sPassword, "1111aaaa"); //设备登录密码
    // //设备信息, 输出参数
    // NET_DVR_DEVICEINFO_V40 struDeviceInfoV40 = {0};
    // lUserID = NET_DVR_Login_V40(&struLoginInfo, &struDeviceInfoV40);
    // if (lUserID < 0) {
    //     printf("Login failed, error code: %d\n", NET_DVR_GetLastError());
    //     NET_DVR_Cleanup();
    //     return 1;
    // }
    // //设置报警回调函数
    // NET_DVR_SetDVRMessageCallBack_V31(MessageCallback, NULL);
    // //启用布防
    // LONG lHandle;
    // NET_DVR_SETUPALARM_PARAM struAlarmParam={0};
    // struAlarmParam.dwSize=sizeof(struAlarmParam);
    // struAlarmParam.byFaceAlarmDetection = 1; //人脸侦测报警,设备支持人脸侦测功能的前提下,上传COMM_ALARM_FACE_DETECTION类型报警信息
    // //其他报警布防参数不需要设置,不支持
    // lHandle = NET_DVR_SetupAlarmChan_V41(lUserID, & struAlarmParam);
    // if (lHandle < 0) {
    //     printf("NET_DVR_SetupAlarmChan_V41 error, %d\n", NET_DVR_GetLastError());
    //     NET_DVR_Logout(lUserID);
    //     NET_DVR_Cleanup();
    //     return 1;
    // }

    // this_thread::sleep_for(std::chrono::milliseconds(50000));
    std::promise<void>().get_future().wait(); 
    // Sleep(50000); //等待过程中,如果设备上传报警信息,在报警回调函数里面接收和处理报警信息
    //撤销布防上传通道
    // if (!NET_DVR_CloseAlarmChan_V30(lHandle)) {
    //     printf("NET_DVR_CloseAlarmChan_V30 error, %d\n", NET_DVR_GetLastError());
    //     NET_DVR_Logout(lUserID);
    //     NET_DVR_Cleanup();
    //     return 1;
    // }

    //注销用户
    // NET_DVR_Logout(lUserID);
    //释放SDK资源
    // NET_DVR_Cleanup();
    return 0;
}
